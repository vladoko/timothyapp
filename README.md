# TimothyApp

This is the application for my son's tablet. With it he can:
* Play sounds of animals, by clicking on their icons
* Play a game of colors (where he can touch the display and the background color changes with sound
* Fall asleep easlily by selecting different white noise loops (rain, waves, fire, nature) 

## TODO

What I would like to have in the future:

- [ ] Timer for the sleeping sounds (select predefined duration of the loops)
- [ ] Animated game when he get's older
- [ ] to be filled later

## About distribution

TimothyApp was born from the frustration that apps for children are still bloathed with ads, paywalls etc. I totally understand that developers need to be paied for their work, but I also think that if you are giving something for free you should not expect a reward or force your reward on children.

The app is not intendet to be distributed, although if I manage to collect in one page the information for the images and sounds that I have included, it will be possible to publish the app on the App Store, but currently this is not my priority. All the assets were free to use, but if someone thinks that I am violating their right it some way, feel free to contact me.

Since I'm ne to iOS development and Swift, don't expect code that is high quality or an example for best practicies. In the end - it is developed only to help my wife and kid, but if someone finds it usefull - feel free to contribute. But remember - if the app gets published it will be free without any ads, paywalls etc.
