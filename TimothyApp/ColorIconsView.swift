//
//  ColorIconsView.swift
//  TimothyApp
//
//  Created by Vladimir Kolev on 22.03.23.
//

import SwiftUI

struct ColorIconsView: View {
    @State private var backgroundColor: Color? = Color.white
    @State private var foregroundColor: Color? = Color.black
    @State private var iconName: String? = "sun.min"
    private var soundPlayer = SoundPlayer(soundPath: Bundle.main.path(forResource: "ping", ofType: "mp3"))
    var body: some View {
        ZStack {
            Image(systemName: iconName!)
                .resizable(resizingMode: .stretch)
                .foregroundColor(foregroundColor)
                .padding(100.0)
        }.background(backgroundColor)
            .onTapGesture {
                soundPlayer.play()
                backgroundColor = getRandomColor()
                foregroundColor = getRandomColor()
                iconName = getRandomIcon()
            }
            .navigationBarTitleDisplayMode(.inline)
            .padding()
            .frame(
                  minWidth: 0,
                  maxWidth: .infinity,
                  minHeight: 0,
                  maxHeight: .infinity,
                  alignment: .topLeading
                )
    }
    
    func getRandomColor() -> Color {
        return Color(
            red: .random(in: 0...1),
            green: .random(in: 0...1),
            blue: .random(in: 0...1)
        )
    }
    
    func getRandomIcon() -> String {
        let iconChoices = [
            "globe.americas",
            "globe.europe.africa",
            "globe.asia.australia",
            "globe.central.south.asia"
        ]
        return iconChoices.randomElement() ?? "sun.min"
    }
}

struct ColorIconsView_Previews: PreviewProvider {
    static var previews: some View {
        ColorIconsView()
    }
}
