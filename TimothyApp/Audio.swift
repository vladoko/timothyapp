//
//  Audio.swift
//  TimothyApp
//
//  Created by Vladimir Kolev on 03.12.22.
//

import Foundation
import AVKit

class SoundPlayer {
    
    var soundPath: String?
    
    init(soundPath: String?) {
        self.soundPath = soundPath
    }
    
    private var audioPlayer: AVAudioPlayer!
    func play() {
        self.audioPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: self.soundPath!))
        self.audioPlayer.play()
    }
    
    func setVolume(value: Float) {
        if (self.audioPlayer != nil ) {
            self.audioPlayer.setVolume(value, fadeDuration: 0)
        }
    }
    
    func playLooped(volume: Float) {
        self.audioPlayer = try! AVAudioPlayer(contentsOf: URL(fileURLWithPath: self.soundPath!))
        self.audioPlayer.numberOfLoops = -1
        self.audioPlayer.setVolume(volume, fadeDuration: 0)
        self.audioPlayer.play()
    }
    
    func stop() {
        self.audioPlayer.stop()
    }
    
    func isPlaying() -> Bool {
        return self.audioPlayer.isPlaying
    }
}
