//
//  AnimalsMainView.swift
//  TimothyApp
//
//  Created by Vladimir Kolev on 21.03.23.
//

import SwiftUI

struct AnimalsMainView: View {
    @State private var selectedTab = 0
    var body: some View {
        TabView(selection: $selectedTab) {
JungleView()
                .tag(0)
.tabItem {
    Label("Jungle", systemImage: "leaf")
    Text("Jungle").font(.largeTitle).fontWeight(.heavy).foregroundColor(Color.black)
}
FarmView()
                .tag(1)
.tabItem{
    Label("Farm", systemImage: "tree")
    Text("Farm")
        .font(.largeTitle)
        .fontWeight(.heavy)
        .foregroundColor(Color.black)
}
AnimalsView()
                .tag(2)
.tabItem {
    Label("Animals", systemImage: "pawprint")
    Text("Animals")
        .font(.largeTitle)
        .fontWeight(.heavy)
        .foregroundColor(Color.black)
}
}
.padding()
.tabViewStyle(PageTabViewStyle())
    }
}

struct AnimalsMainView_Previews: PreviewProvider {
    static var previews: some View {
        AnimalsMainView()
    }
}
