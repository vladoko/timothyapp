//
//  SleepSound.swift
//  TimothyApp
//
//  Created by Vladimir Kolev on 21.03.23.
//

import Foundation

class SleepSound: Hashable, Identifiable {
    var id: UUID = UUID()
    var name: String
    var icon: String
    var sound: String?
    var player: SoundPlayer
    
    init(name: String, icon: String, sound: String) {
        self.name = name
        self.icon = icon
        self.sound = Bundle.main.path(forResource: sound, ofType: "mp3")
        self.player = SoundPlayer(soundPath: self.sound)
    }
    
    static func == (lhs: SleepSound, rhs: SleepSound) -> Bool {
        lhs.name == rhs.name
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.name)
    }
}
