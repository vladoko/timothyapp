//
//  TimothyAppApp.swift
//  TimothyApp
//
//  Created by Vladimir Kolev on 01.12.22.
//

import SwiftUI

@main
struct TimothyAppApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
