//
//  Animal.swift
//  TimothyApp
//
//  Created by Vladimir Kolev on 03.12.22.
//

import Foundation


class Animal : Hashable {
    var name: String
    var sound: String?
    var image: String
    var player: SoundPlayer
    
    init(name: String, sound: String) {
        self.name = name
        self.sound = Bundle.main.path(forResource: sound, ofType: "mp3")
        self.image = name.lowercased()
        self.player = SoundPlayer(soundPath: self.sound)
    }
    
    static func == (lhs: Animal, rhs: Animal) -> Bool {
        lhs.name == rhs.name
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.name)
    }
}


let farmAnimals = [
    Animal(name: "horse", sound: "horse"),
    Animal(name: "pig", sound: "pig"),
    Animal(name: "sheep", sound: "sheep"),
    Animal(name: "caw0", sound: "cow"),
    Animal(name: "bull", sound: "ochse"),
    Animal(name: "chicken", sound: "chicken")
]

let jungleAnimals = [
    Animal(name: "lion", sound: "lion"),
    Animal(name: "monkey", sound: "monkey"),
    Animal(name: "tiger", sound: "tiger"),
    Animal(name: "elephant", sound: "elephant"),
    Animal(name: "bear", sound: "bear"),
    Animal(name: "owl", sound: "owl")
]

let homeAnimals = [
    Animal(name: "cat", sound: "cat"),
    Animal(name: "dog", sound: "dog"),
    Animal(name: "donkey", sound: "donkey")
]
