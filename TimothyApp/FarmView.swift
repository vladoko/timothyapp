//
//  FarmView.swift
//  TimothyApp
//
//  Created by Vladimir Kolev on 01.12.22.
//

import SwiftUI
import AVKit


struct FarmView: View {
    private var gridItemLayout = Array(repeating: GridItem(.flexible(), spacing: 0.0), count: 3)
    var body: some View {
        ZStack {
            LazyVGrid(columns: gridItemLayout, spacing: 100.0) {
                ForEach(farmAnimals, id: \.self) {item in
                    Image(item.image)
                        .resizable(resizingMode: .stretch)
                        .frame(width: 200, height: 200)
                        .shadow(radius: 10.0)
                        .onTapGesture {
                            item.player.play()
                        }
                }

            }
        }
        .navigationBarTitleDisplayMode(.inline)
        .padding(.top, 100.0)
        .frame(
              minWidth: 0,
              maxWidth: .infinity,
              minHeight: 0,
              maxHeight: .infinity,
              alignment: .topLeading
            )
        .background(
            Image("farm-landscape")
                .resizable()
                .scaledToFill()
        )
    }
}

struct FarmView_Previews: PreviewProvider {
    static var previews: some View {
        FarmView()
            .previewInterfaceOrientation(.landscapeRight)
            .previewLayout(.fixed(width: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/))
            .previewDevice("iPad Air (5th generation)")
    }
}
