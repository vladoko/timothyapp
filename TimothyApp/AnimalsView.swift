//
//  AnimalsView.swift
//  TimothyApp
//
//  Created by Vladimir Kolev on 01.12.22.
//

import SwiftUI
import AVKit


struct AnimalsView: View {
    private var gridItemLayout = Array(repeating: GridItem(.flexible(), spacing: 0.0), count: 3)
    var body: some View {
        ZStack {
            LazyVGrid(columns: gridItemLayout, spacing: 100.0) {
                ForEach(homeAnimals, id: \.self) { animal in
                    Image(animal.image)
                        .resizable(resizingMode: .stretch)
                        .frame(width: 200, height: 200)
                        .shadow(radius: 10.0)
                        .onTapGesture {
                            animal.player.play()
                        }
                }
                
            }
            .padding(.top, 100.0)
        }
        .navigationBarTitleDisplayMode(.inline)
        .frame(
              minWidth: 0,
              maxWidth: .infinity,
              minHeight: 0,
              maxHeight: .infinity,
              alignment: .topLeading
            )
        .background(
            Image("home-landscape")
                .resizable()
                .scaledToFill()
        )
    }
}

struct AnimalsView_Previews: PreviewProvider {
    static var previews: some View {
        AnimalsView()
            .previewInterfaceOrientation(.landscapeRight)
            .previewLayout(.fixed(width: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/))
            .previewDevice("iPad Air (5th generation)")
    }
}
