//
//  SleepMainView.swift
//  TimothyApp
//
//  Created by Vladimir Kolev on 21.03.23.
//

import SwiftUI

struct CustomSliderPlayer: View {
    @State var sliderValue: Double = 0
    @State private var playing: Bool = false
    var sleepSound: SleepSound
    var body: some View {
        HStack(alignment: .center, spacing: 10.0) {
            Image(systemName: sleepSound.icon)
                .foregroundColor(Color.white)
                .frame(width: /*@START_MENU_TOKEN@*/50.0/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/10.0/*@END_MENU_TOKEN@*/)
                .imageScale(.large)
            Text(sleepSound.name).font( .headline).fontWeight(.bold).foregroundColor(Color.white).multilineTextAlignment(.leading).frame(width: 100, height: 10.0)
            Slider(value: $sliderValue, in: 0...1)
                .onChange(of: sliderValue) { value in
                    sleepSound.player.setVolume(value: Float(sliderValue))
                }
            Toggle("", isOn: $playing).onChange(of: playing) { value in
                if (value == true) {
                    sleepSound.player.playLooped(volume: Float(sliderValue))
                } else {
                    sleepSound.player.stop()
                }
                
            }
        }.padding(5)
    }
}



struct SleepMainView: View {
    @State private var value: Double = 0.0
    @State var minutes: String
    let sounds = [
        SleepSound(name: "Raining", icon: "cloud.rain.fill", sound: "raining"),
        SleepSound(name: "Sea", icon: "bolt.horizontal.fill",
                   sound:"sea"),
        SleepSound(name: "River", icon: "drop.fill", sound:"river"),
        SleepSound(name: "Birds", icon: "paperplane", sound:"birds"),
        SleepSound(name: "White", icon: "bolt.fill", sound: "white-noise"),
        SleepSound(name: "Fire", icon: "flame.fill", sound: "fire")
    ]
    let timerValues = ["15", "30", "45", "60"]
    @State private var timerSelected = "30"
    var body: some View {
        VStack(alignment: .center, spacing: 30.0) {
            ForEach(sounds) { sound in
                CustomSliderPlayer(sleepSound: sound)
            }
            VStack(alignment: .center) {
                HStack {
                    Text("Set duration timer:")
                        .font(.largeTitle)
                        .foregroundColor(Color.white)
                }
                HStack(alignment: .center) {
                    Text("Minutes").font(.title).foregroundColor(Color.white)
                    Picker("MM", selection: $timerSelected) {
                        ForEach(timerValues, id: \.self) {
                            Text($0)
                        }
                    }.pickerStyle(.segmented).padding(15.0)
                    Button(action: {
                        for sound in sounds {
                            if (sound.player.isPlaying()) {
                                sound.player.stop()
                            }
                        }
                    }) {
                        Text("SET")
                            .buttonStyle(.bordered)
                            .controlSize(.large)
                            .foregroundColor(Color.white)
                            .padding(15)
                            .background(RoundedRectangle(cornerRadius: 14.0, style: .continuous).fill(Color.accentColor))
                    }
                }
            }
            .padding([.top, .leading, .trailing], 50.0)
        }
        .navigationBarTitleDisplayMode(.inline)
        .padding()
        .frame(
              minWidth: 0,
              maxWidth: .infinity,
              minHeight: 0,
              maxHeight: .infinity,
              alignment: .topLeading
            )
        .background(Image("sleepbackground")
            .resizable()
            .scaledToFill())
        
    }
}

struct SleepMainView_Previews: PreviewProvider {
    static var previews: some View {
        SleepMainView(minutes: "00")
    }
}
