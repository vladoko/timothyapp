//
//  ContentView.swift
//  TimothyApp
//
//  Created by Vladimir Kolev on 01.12.22.
//

import SwiftUI
import AVFAudio

struct MenuItem: Identifiable {
    var id = UUID()
    var name: String
    var icon: String
    var tag: Int
    var view: AnyView
}

struct ContentView: View {
    
    let menuItems: [MenuItem] = [
        MenuItem(name: "Animals", icon: "hare", tag: 0, view: AnyView(AnimalsMainView())),
        MenuItem(name: "Sleep", icon: "moon.stars", tag: 1,
                 view: AnyView(SleepMainView(minutes: "00"))),
        MenuItem(name: "Colors", icon: "paintpalette",
                 tag: 2,
                 view: AnyView(ColorIconsView())),
    ]
    
    @State private var selectedView: Int? = 0
    
    var body: some View {
        NavigationView {
            List(menuItems, selection: $selectedView) {menuItem in
                NavigationLink(destination: menuItem.view) {
                    HStack{
                        Image(systemName: menuItem.icon)
                        Text(menuItem.name)
                    }
                }
            }
            
            AnimalsMainView()
                
            }.navigationTitle("Navigation")
                .navigationBarTitleDisplayMode(.inline)
                .onAppear {
                    let session = AVAudioSession.sharedInstance()
                    do {
                        try session.setActive(true)
                        try session.setCategory(.playback, mode: .default, options: .defaultToSpeaker
                        )
                    } catch {
                        print(error.localizedDescription)
                    }
                }
//                .navigationSplitViewStyle(.prominentDetail)
    }
    }


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .previewInterfaceOrientation(.landscapeRight)
            .previewLayout(.fixed(width: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/))
            .previewDevice("iPad Air (5th generation)")
    }
}
